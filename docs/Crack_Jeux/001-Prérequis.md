# Prérequis

Dans l'entièreté des téléchargements de cracks, il vous faudra le logiciel WinRar. Le logiciel est téléchargable [ici](https://www.win-rar.com/fileadmin/winrar-versions/winrar/winrar-x64-622.exe)

(L'auteur de ces lignes vous conseille tout de même [7-zip](https://7-zip.org/a/7z2301-x64.exe) qui est plus léger, fonctionne de la même manière et entièrement gratuit.)