# Subnautice : Below Zero

![](https://cdn.discordapp.com/attachments/990257073214337065/990257941087154247/subnautica_below_zero.jpg)

Lien du téléchargement : [https://mega.nz/file/a25QxKYR#VH5EeM7D3ChjyxEtvzXOI_48EXdgdUA27xyVGj8BcaA](https://mega.nz/file/a25QxKYR#VH5EeM7D3ChjyxEtvzXOI_48EXdgdUA27xyVGj8BcaA)

Clé de déchiffrement (si nécessaire) : `VH5EeM7D3ChjyxEtvzXOI_48EXdgdUA27xyVGj8BcaA`

!!! bug "Information importante"
    Si quand vous appuyez sur "Subnautica Below Zero" et que ça ne marche c'est que vous avez Steam donc si vous ne voulez pas le désinstaller alors appuyez sur "Laucher" et ça vous lancera le jeu ! - IWinZar (Non testé par l'auteur)