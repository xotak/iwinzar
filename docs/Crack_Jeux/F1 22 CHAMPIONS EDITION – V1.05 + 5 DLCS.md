# F1 22: CHAMPIONS EDITION – V1.05 + 5 DLCS

Lien du téléchargement : [https://www.clictune.com/iy2D](https://www.clictune.com/iy2D)

!!! warning "Torrent"
    
    Ce téléchargement requiert l'utilisation d'un logiciel de téléchargement compatible avec le protocole [BitTorrent](https://fr.wikipedia.org/wiki/BitTorrent) tel que [qBitTorrent](https://www.qbittorrent.org/), [uTorrent](https://www.utorrent.com/) ou Transmission pour les vrais (:eyes:), accompagné d'un VPN. L'auteur n'est responsable d'aucun problème lié a l'utilisation de ces logiciels ~~et d'un éventuel courrier de l'ARCOM~~. Des bisous.

## Installation

Une fois le dossier téléchargé, lancer l'application `setup` et suivre les étapes.