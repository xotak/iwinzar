# Liste de site de streaming en vrac

- [Wiflix](https://wiflix.studio/)
- [HDS-Streaming](https://www.hds-streaming.cam/)
- [Vostfree](https://vostfree.ws/)
- [OtakuFR](https://otakufr.co/)
- [VoirAnime](https://v5.voiranime.com/) (IMO celui qui marche le mieux et le plus à jour)
- [Anime-Sama](https://anime-sama.fr/)